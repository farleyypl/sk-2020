## System operacyjny w środowisku sieciowym

### Zadania


1. Z wykorzystaniem maszyny wirtualnej, zainstaluj SO oraz wypisz parametry konfiguracji IP tj:
   * Adres
   * Maska
   * Adres bramy
   * DNS 1
   * DNS 2
    
    Powyższe parametry uzyskaj na wszystkich z wymienionych systemów

   * [Linux Alpine](https://alpinelinux.org/)
   * [Linux Debian](https://www.debian.org/)
   * [Linux CentOS](https://www.centos.org/)
   * Windows 

2. Sprawdź oraz przygotuj charakterystykę dla przykładowego urządzenia w Twojej sieci domowej
   * Adres
   * Maska
   * Adres bramy
   * DNS 1
   * DNS 2
  
    Przygotuj dokumentację graficzną Twojej sieci domowej, uwzględnij adresy i urządzenia

3. Zarejestruj konto w CISCO Academy celem pobrania Packet tracer 
   https://www.netacad.com/courses/packet-tracer

4. Dlaczego umiejętnosci z zakresu sieci komputerowych mogą mi się przydać? :)

## Alpine

### Charakterystyka systemu operacyjnego

| Charakterystyka           | wartość               | komentarzu                |
| -------------             |:-------------:        | -----:                    |
| nazwa                     | linux                 | centos 7                  |
| cfg interfejsów           | centos 7 | /etc/sysconfig/network-scripts         |
| program (parametry sieci) | niewiem               |                           |
| ....                      | .....                 |                           |
| nazwa                     | Alpine Linux          |                           |
| Konfiguracja ip           | ``$ ip all ``         | show all eth interfaces   | 
| Tablica routingu          | ``$ ip route show ``  | what is gateway?!         | 
| check nameservers (DNS)   | ``$ cat /etc/resolv.conf ``  | which DNS were set | 

### Konfiguracja połączenia sieciowego

| Parametr | wartość           | komentarzu |
| ------------- |:-------------:| -----:|
| Adres IP      | 10.0.2.15        | przydzielony przez DHCP |
| Maska podsieci| 10.0.2.15/**24** | **255.255.255.0**    |
| Brama         | 10.0.2.2         | default from route table |
| DNS 1         | 192.168.0.1      | cat /etc/resolv.conf     |
| DNS 2         | 192.168.0.1          | nslookup uek.krakow.pl   |



## macOS Mojave
| Charakterystyka           | wartość               | komentarzu                |
| -------------             |:-------------:        | -----:                    |
| nazwa                     | macOS                 |        wersja 10.14.6     |
| cfg interfejsów           |                       |         Co to?            |
| program (parametry sieci) |                       |                           |
| nazwa                     | macOS Mojave          |                           |
| Konfiguracja ip           | ``$ if config ``      |                           | 
| Tablica routingu          | ``netstat -nr ``      |                           | 
| check nameservers (DNS)   | ``scutil --dns ``     |                           | 

### Konfiguracja połączenia sieciowego
| Parametr | wartość           | komentarzu |
| ------------- |:-------------:| -----:|
| Adres IP      | 192.168.0.100        | przydzielony przez DHCP |
| Maska podsieci| 0xffffff00 | **255.255.255.0**    |
| Brama         | 192.168.0.1          |  |
| DNS 1         |  192.168.0.1      |    |
| DNS 2         |  192.168.0.1      |    |


Użyte komendy:  
```
$ ifconfig | grep "inet" | grep -v 127.0.0.1
   inet 192.168.0.100 netmask 0xffffff00 broadcast 192.168.0.255
```

```
$ scutil --dns | grep 'nameserver\[[0-9]*\]'
  nameserver[0] : 192.168.0.1
  nameserver[0] : 192.168.0.1
```

```
$ netstat -nr | grep default
default            192.168.0.1        UGSc          124        0     en0
```



### Schemat sieci

aby załączyć obrazek 

```markdown
![alt schemat](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png)![alt schemat](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png)

![alt schemat](images/my-network-schema.png)
```

![my network](network.png)

